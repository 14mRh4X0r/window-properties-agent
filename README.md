# Window properties agent for Java

This little Java agent lets you set X11 window properties of type `STRING` or `UTF8_STRING` on all
windows created by the application.

Usage:

```
java -javaagent:window-properties-agent-1.0-SNAPSHOT.jar=[debug,]<property>[:<type>]=<value>[,…] …
```

`<type>` can be `u` for `UTF8_STRING` properties or `s` for `STRING` properties (the default).

## Examples

To set the GTK window decorations to the dark theme:

```
java -javaagent:window-properties-agent-1.0-SNAPSHOT.jar=_GTK_THEME_VARIANT:u=dark …
```

## Building

To build the agent, simply run `./gradlew build`, the output will be in `build/libs/`.
