/*
 * Copyright 2019 Willem Mulder
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */
package me.l4mrh4x0r.windowproperties;

import org.objectweb.asm.*;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

import java.io.PrintWriter;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WindowPropertiesAgent implements ClassFileTransformer {
    private final Map<String, Map.Entry<String, String>> PROP_TO_METHOD_VALUE = new HashMap<>();
    private boolean debug = false;
    private boolean dump = false;

    public WindowPropertiesAgent(String props) {
        for (String propTypeVal : props.split(",")) {
            if (propTypeVal.toLowerCase(Locale.ROOT).equals("debug")) {
                System.err.println("WindowPropertiesAgent debugging turned on");
                debug = true;
                continue;
            } else if (propTypeVal.toLowerCase(Locale.ROOT).equals("dump")) {
                System.err.println("WindowPropertiesAgent dumping turned on");
                dump = true;
                continue;
            }

            String[] split = propTypeVal.split("=", 2);
            String value = split[1];

            split = split[0].split(":", 2);
            String property = split[0];
            String type = split.length > 1 ? split[1] : "s";

            String method;
            if ("u".equals(type)) {
                method = "setPropertyUTF8";
            } else {
                method  = "setProperty8";
            }

            PROP_TO_METHOD_VALUE.put(property, new AbstractMap.SimpleImmutableEntry<>(method, value));
        }
    }

    public static void premain(String agentArgs, Instrumentation instrumentation) {
        instrumentation.addTransformer(new WindowPropertiesAgent(agentArgs));
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain, byte[] classfileBuffer) {
        try {
            if (!"sun/awt/X11/XBaseWindow".equals(className)) {
                return null;
            }

            if (debug) System.err.println("Patching sun/awt/X11/XBaseWindow…");

            ClassReader reader = new ClassReader(classfileBuffer);
            ClassWriter writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
            TraceClassVisitor visitor = new TraceClassVisitor(writer, new PrintWriter(System.err, true));
            XWindowAdapter adapter = new XWindowAdapter(dump ? visitor : writer, debug);

            reader.accept(adapter, 0);

            return writer.toByteArray();
        } catch (Exception e) {
            System.err.println("Exception while patching: " + e);
            e.printStackTrace();
            throw e;
        }
    }

    private class XWindowAdapter extends ClassVisitor {
        private final boolean debug;

        public XWindowAdapter(ClassVisitor visitor, boolean debug) {
            super(ASM8, visitor);
            this.debug = debug;
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
            MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);

            if (mv != null && "postInit".equals(name) && "(Lsun/awt/X11/XCreateWindowParams;)V".equals(desc)) {
                if (debug) System.err.println("Patching postInit(Lsun/awt/X11/XCreateWindowParams;)V…");
                mv = new PostInitAdapter(mv);
            }

            return mv;
        }
    }

    private class PostInitAdapter extends MethodVisitor {
        public PostInitAdapter(MethodVisitor visitor) {
            super(ASM8, visitor);
        }

        @Override
        public void visitInsn(int opcode) {
            // Insert our code before end of the method
            if (opcode != RETURN) {
                super.visitInsn(opcode);
                return;
            }

            Label start = new Label();
            Label end = new Label();
            Label handler = new Label();

            // XToolKit.awtLock();
            visitMethodInsn(INVOKESTATIC, "sun/awt/X11/XToolkit", "awtLock", "()V", false);

            // try {
            visitTryCatchBlock(start, end, handler, null);
            visitLabel(start);

            // long windowId = this.getWindow();
            visitVarInsn(ALOAD, 0);
            visitMethodInsn(INVOKEVIRTUAL, "sun/awt/X11/XBaseWindow", "getWindow", "()J", false);
            visitVarInsn(LSTORE, 1);

            for (Map.Entry<String, Map.Entry<String, String>> propMethodValue : PROP_TO_METHOD_VALUE.entrySet()) {
                // XAtom atom = XAtom.get(property);
                String property = propMethodValue.getKey();
                Map.Entry<String, String> methodValue = propMethodValue.getValue();
                String method = methodValue.getKey();
                String value = methodValue.getValue();

                visitLdcInsn(property);
                visitMethodInsn(INVOKESTATIC, "sun/awt/X11/XAtom",
                        "get", "(Ljava/lang/String;)Lsun/awt/X11/XAtom;", false);


                // atom.<method>(windowId, value);
                visitVarInsn(LLOAD, 1);
                visitLdcInsn(value);
                visitMethodInsn(INVOKEVIRTUAL, "sun/awt/X11/XAtom",
                        method, "(JLjava/lang/String;)V", false);
            }

            // } finally (no exception) {
            visitLabel(end);
            visitMethodInsn(INVOKESTATIC, "sun/awt/X11/XToolkit", "awtUnlock", "()V", false);
            super.visitInsn(RETURN);

            // } finally (exception) {
            visitLabel(handler);
            visitMethodInsn(INVOKESTATIC, "sun/awt/X11/XToolkit", "awtUnlock", "()V", false);
            super.visitInsn(ATHROW);
            // }
        }
    }
}
